﻿using UnityEngine;


public class CameraMove : MonoBehaviour
{
    private Vector3 dragOrigin; //Where are we moving?
    private Vector3 clickOrigin = Vector3.zero; //Where are we starting?
    private Vector3 basePos = Vector3.zero; //Where should the camera be initially?

    Transform myTransform;

    public float MinY;
    public float MaxY;

    float Speed;
    float Damping = 2f;

    float y;

    Vector3 newPos = new Vector3();

    void Start()
    {

        myTransform = transform;

    }


    void LateUpdate()
    {

        if (Speed>0)
        {
            Speed -= Time.deltaTime*Damping;


            basePos = myTransform.position;

            y = Mathf.Clamp(basePos.y + Speed * Time.deltaTime, MinY, MaxY);

            newPos.Set(basePos.x, y, -10);
            myTransform.position = newPos;
        }

        else if (Speed < 0)
        {
            Speed += Time.deltaTime * Damping;


            basePos = myTransform.position;

            y = Mathf.Clamp(basePos.y + Speed*Time.deltaTime, MinY, MaxY);

            newPos.Set(basePos.x, y, -10);
            myTransform.position = newPos;

        }
        
 
    }



    public void SetSpeed(float speed)
    {

        Speed = speed;
    }
}
