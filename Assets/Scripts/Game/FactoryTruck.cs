﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum MovingFactoryTruckState { ToPoint, ToFactory, Loading };

public class FactoryTruck : MonoBehaviour
{

    [SerializeField]
    int TrashMass;
    int StartMassHelper;


    [SerializeField]
    public int MaxTrashMass;

    [SerializeField]
    public float Speed = -0.02f;

    float checkPeriod = 0.1f;
    float timeTemp;

    private int lastCheckedPoint = 0;

    MovingFactoryTruckState currentState;

    Vector2 speed;
    Rigidbody2D rig;
    SpriteRenderer sr;

    public string name = "Factory Truck";

    void Start()
    {
        currentState = MovingFactoryTruckState.ToPoint;

        speed = new Vector2(0, Speed);

        rig = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        if (timeTemp > 0)
        {
            timeTemp -= Time.deltaTime;
        }
        else
        {
            CheckArrival();

            timeTemp = checkPeriod;
        }




    }

    void FixedUpdate()
    {
        if (currentState != MovingFactoryTruckState.Loading)
        {
            rig.MovePosition(rig.position + speed);
        }

    }



    private void CheckArrival()
    {

        switch (currentState)
        {

            case MovingFactoryTruckState.ToFactory:

                sr.flipY = false;

                CheckFactory();
                break;

            case MovingFactoryTruckState.ToPoint:

                sr.flipY = true;

                CheckPoint();
                break;
        }

    }



    private void CheckPoint()
    {
        Vector3 rayPosition = new Vector3(rig.position.x, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), new Vector2(1,0), 3f);

        Debug.DrawRay(rayPosition, new Vector2(1, 0), Color.red);

        if (hit)
        {
            var hitObj = hit.collider.gameObject.GetComponent<TakeUpPoint>();

            if (hitObj != null)
            {
               

                if(lastCheckedPoint!= hitObj.GetIndex())
                {
                    int res = hitObj.LoadTrash(StartMassHelper / GameManager.instance.GetIndex());
    
                    TrashMass -= StartMassHelper / GameManager.instance.GetIndex();
                   
                    TrashMass += res;
                   
                }

               

                if (hitObj.GetIndex() == GameManager.instance.GetIndex())
                {
                   
                    int result = hitObj.LoadTrash(StartMassHelper % GameManager.instance.GetIndex());

                    Debug.Log(StartMassHelper % GameManager.instance.GetIndex());

                    TrashMass -= StartMassHelper % GameManager.instance.GetIndex();

                    TrashMass += result ;


                    speed *= -1;
                    StartCoroutine(Loading(MovingFactoryTruckState.ToFactory));
                }
                else if(lastCheckedPoint != hitObj.GetIndex())
                {
                    StartCoroutine(Loading(MovingFactoryTruckState.ToPoint));
                    lastCheckedPoint = hitObj.GetIndex();
                }
                  
            }


        }

    }


    private void CheckFactory()
    {
        Vector3 rayPosition = new Vector3(rig.position.x - 0.5f, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), new Vector2(0,1), 1f);

        Debug.DrawRay(rayPosition, speed * 5, Color.red);

        if (hit)
        {

            var hitObj = hit.collider.gameObject.GetComponent<Factory>();

            if (hitObj != null)
            {

                TrashMass += hitObj.GetTrash(MaxTrashMass - TrashMass);

                StartMassHelper = TrashMass;


                speed *= -1;

                lastCheckedPoint = 0;

                StartCoroutine(Loading(MovingFactoryTruckState.ToPoint));
            }

        }
    }



    private IEnumerator Loading(MovingFactoryTruckState nextState)
    {
        currentState = MovingFactoryTruckState.Loading;

        yield return new WaitForSeconds(2f);

        currentState = nextState;

    }
}
