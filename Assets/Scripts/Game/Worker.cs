﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
enum MovingWorkerState { ToPoint, ToDump, Loading, Sleeping };
public class Worker : MonoBehaviour
{

    

    [SerializeField]
    int TrashMass;

    [SerializeField]
    int MaxTrashMass;

    [SerializeField]
    float Speed = 0.05f;

    float checkPeriod = 0.2f;
    float timeTemp;

    MovingWorkerState currentState;

    Vector2 speed;

    [HideInInspector]
    public float speedx;

    Rigidbody2D rig;
    SpriteRenderer sr;

    [SerializeField]
    int indexWorker;

    public string name = "Worker ";

    [SerializeField]
    GameObject UpdateButton;

    [SerializeField]
    GameObject UpdateButtonPoint;

    [SerializeField]
    GameObject UpdateButtonDump;

    void Start()
    {
        currentState = MovingWorkerState.ToDump;

        speed = new Vector2(Speed, 0);

        speedx = 0.02f;

        name += indexWorker;

        rig = GetComponent<Rigidbody2D>();

        sr = GetComponent<SpriteRenderer>();

        if (GameManager.instance.GetIndex()<indexWorker)
        {
            Disable();
        }

    }


    private void Enable()
    {
        sr.enabled = true;
        currentState = MovingWorkerState.ToDump;

        UpdateButton.SetActive(true);
        UpdateButtonPoint.SetActive(true);
        UpdateButtonDump.SetActive(true);


    }

    private void Disable()
    {
        sr.enabled = false;
        currentState = MovingWorkerState.Sleeping;

        UpdateButton.SetActive(false);
        UpdateButtonPoint.SetActive(false);
        UpdateButtonDump.SetActive(false);

    }


    void Update()
    {
        if (timeTemp > 0)
        {
            timeTemp -= Time.deltaTime;
        }
        else
        {
            if(currentState != MovingWorkerState.Sleeping)
            CheckArrival();


            if (GameManager.instance.GetIndex() >= indexWorker && currentState == MovingWorkerState.Sleeping)
            {
                Enable();
            }


            timeTemp = checkPeriod;
        }
       

    }

    void FixedUpdate()
    {
        if (currentState != MovingWorkerState.Loading && currentState != MovingWorkerState.Sleeping)
        {
            if(currentState == MovingWorkerState.ToDump)
                rig.MovePosition(rig.position + speed);
            else
                rig.MovePosition(rig.position - speed);
        }

    }



    private void CheckArrival()
    {

        switch (currentState)
        {

            case MovingWorkerState.ToDump:

                sr.flipX = false;

                CheckDump();
                break;

            case MovingWorkerState.ToPoint:

                sr.flipX = true;
                CheckPoint();
                break;
        }

    }



    private void CheckPoint()
    {
        Vector3 rayPosition = new Vector3(rig.position.x - 0.5f, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), -speed * 5, 3f);

        Debug.DrawRay(rayPosition, -speed * 5, Color.red);
        if (hit)
        {
            var hitObj = hit.collider.gameObject.GetComponent<TakeUpPoint>();

            if (hitObj != null)
            {

                TrashMass += hitObj.GetTrash(MaxTrashMass - TrashMass);

                Debug.Log("Worker mass"+TrashMass);

                StartCoroutine(Loading(MovingWorkerState.ToDump));
            }


        }

    }


    private void CheckDump()
    {
        Vector3 rayPosition = new Vector3(rig.position.x + 0.5f, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), speed * 5, 1f);

        Debug.DrawRay(rayPosition, speed * 5, Color.red);

        if (hit)
        {

            var hitObj = hit.collider.gameObject.GetComponent<Dump>();

            if (hitObj != null)
            {

                TrashMass = hitObj.LoadTrash(TrashMass);

                StartCoroutine(Loading(MovingWorkerState.ToPoint));
            }

        }
    }



    private IEnumerator Loading(MovingWorkerState nextState)
    {
        currentState = MovingWorkerState.Loading;

        yield return new WaitForSeconds(2f);

        currentState = nextState;

    }
}
