﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Factory : MonoBehaviour
{
    [SerializeField]
    int Capacity;

    TextMesh text;

    private int currentTrashLoaded;

    public string name = "Factory";

    void Start ()
    {
        currentTrashLoaded = PlayerPrefs.GetInt("currentTrashLoaded2");
        text = GetComponentInChildren<TextMesh>();

        UpdateCounter();
    }


    public int LoadTrash(int trash)
    {

        if (Capacity-currentTrashLoaded>=trash)
        {
            currentTrashLoaded += trash;

            UpdateCounter();

            return 0;
        }
        else
        {
            currentTrashLoaded = Capacity; 


            UpdateCounter();

            return trash-(Capacity - currentTrashLoaded);

        }   
    }


    public int GetTrash(int trashNeeded)
    {

        if (currentTrashLoaded >= trashNeeded)
        {
            currentTrashLoaded -= trashNeeded;

            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded2", currentTrashLoaded);
            return trashNeeded;
        }
        else
        {
            int trash = currentTrashLoaded;

            currentTrashLoaded = 0;

            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded2", currentTrashLoaded);
            return trash;
        }

    }


    void UpdateCounter()
    {

        text.text = currentTrashLoaded + "/" + Capacity;

    }
}
