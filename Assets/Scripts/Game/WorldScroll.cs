﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WorldScroll : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{


    Vector2 startPoint;
    Vector2 endPoint;

    CameraMove cam;


    void Awake()
    {
        Application.targetFrameRate = 30;
    }

    void Start ()
    {
        cam = FindObjectOfType<CameraMove>();
     
	}
	

    public void OnDrag(PointerEventData eventData)
    {

        cam.SetSpeed((startPoint.y-eventData.position.y)/20f);

    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startPoint = eventData.position;

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        endPoint = eventData.position;


        cam.SetSpeed((startPoint.y-endPoint.y)/10f);
    }
}
