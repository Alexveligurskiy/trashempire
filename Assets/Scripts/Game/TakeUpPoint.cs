﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeUpPoint : MonoBehaviour
{

    [SerializeField]
    int Capacity;

    TextMesh text;

    private int currentTrashLoaded;


    [SerializeField]
    int index;

    public string name = "Point ";

	void Start ()
    {
        currentTrashLoaded = PlayerPrefs.GetInt("currentTrashLoaded3");
        text = GetComponentInChildren<TextMesh>();

        name += index;

        UpdateCounter();

    }
	

    public  int GetIndex()
    {
        return index;
    }

    public int LoadTrash(int trash)
    {

        if (Capacity - currentTrashLoaded >= trash)
        {
            currentTrashLoaded += trash;

            UpdateCounter();

            return 0;
        }
        else
        {
            currentTrashLoaded = Capacity;


            UpdateCounter();

            return trash - (Capacity - currentTrashLoaded);

        }
    }


    public int GetTrash(int trashNeeded)
    {

        if (currentTrashLoaded >= trashNeeded)
        {
            currentTrashLoaded -= trashNeeded;

            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded3", currentTrashLoaded);
            return trashNeeded;
        }
        else
        {
            int trash = currentTrashLoaded;

            currentTrashLoaded = 0;

            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded3", currentTrashLoaded);
            return trash;
        }

    }

    void UpdateCounter()
    {

        text.text = currentTrashLoaded + "/" + Capacity;

    }


	
}


