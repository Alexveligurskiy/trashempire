﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : MonoBehaviour
{
    [SerializeField]
    int TrashPerSecond;

    [SerializeField]
    int Capacity;

    TextMesh text;

    private int currentTrashLoaded;

    private float timer;

    public string name = "City";

    void Start ()
    {
        currentTrashLoaded = PlayerPrefs.GetInt("currentTrashLoaded0");
        text = GetComponentInChildren<TextMesh>();

        timer = 1f;
        	
	}



    public int GetTrash(int trashNeeded)
    {

        if (currentTrashLoaded >= trashNeeded)
        {
            currentTrashLoaded -= trashNeeded;

            UpdateCounter();

            return trashNeeded;
        }
        else
        {
            int trash = currentTrashLoaded;

            currentTrashLoaded = 0;

            UpdateCounter();

            return trash;
        }

    }



    void Update()
    {

        if (timer>0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            timer = 1f;

            currentTrashLoaded += TrashPerSecond;

            currentTrashLoaded = Mathf.Clamp(currentTrashLoaded,0,Capacity);
            PlayerPrefs.SetInt("currentTrashLoaded0", currentTrashLoaded);
            UpdateCounter();
        }



    }
	

    void UpdateCounter()
    {

        text.text = currentTrashLoaded+"/"+Capacity;

    }

}
