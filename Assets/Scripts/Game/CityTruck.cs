﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum MovingState {ToSity, ToFactory, Loading};

public class CityTruck : MonoBehaviour
{
    [SerializeField]
    int TrashMass;

    [SerializeField]
    int MaxTrashMass;

    [SerializeField]
    float Speed = -0.05f;

    public string name = "City Truck";

    float checkPeriod = 0.2f;
    float timeTemp;

    MovingState currentState;

    Vector2 speed;
    Rigidbody2D rig;
    SpriteRenderer sr;

    void Start()
    {
        currentState = MovingState.ToFactory;

        speed = new Vector2(Speed,0);

        rig = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        if (timeTemp>0)
        {
            timeTemp -= Time.deltaTime;
        }
        else
        {
            CheckArrival();

            timeTemp = checkPeriod;
        }


        

    }

    void FixedUpdate()
    {
        if(currentState!= MovingState.Loading)
        {
            rig.MovePosition(rig.position + speed);
        }
       
    }



    private void CheckArrival()
    {
        
        switch (currentState)
        {

            case MovingState.ToFactory:

                sr.flipY = true;

                CheckFactory();
                break;

            case MovingState.ToSity:

                sr.flipY = false;

                CheckCity();
                break;
        }

    }



    private void CheckCity()
    {
        Vector3 rayPosition = new Vector3(rig.position.x - 0.5f, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), speed * 5, 1f);

        Debug.DrawRay(rayPosition, speed * 5, Color.red);
        if (hit)
        {
            var hitObj = hit.collider.gameObject.GetComponent<City>();

            if (hitObj != null)
            {
                //currentState = MovingState.ToFactory;
                speed *= -1;

                TrashMass += hitObj.GetTrash(MaxTrashMass-TrashMass);


                StartCoroutine(Loading(MovingState.ToFactory));
            }


        }

    }


    private void CheckFactory()
    {
        Vector3 rayPosition = new Vector3(rig.position.x - 0.5f, rig.position.y);
        RaycastHit2D hit = Physics2D.Raycast(new Vector3(rig.position.x, rig.position.y), speed * 5, 1f);

        Debug.DrawRay(rayPosition, speed * 5, Color.red);

        if (hit)
        {

            var hitObj = hit.collider.gameObject.GetComponent<Factory>();

            if (hitObj != null)
            {
                //currentState = MovingState.ToSity;

                TrashMass = hitObj.LoadTrash(TrashMass);

                speed *= -1;

                StartCoroutine(Loading(MovingState.ToSity));
            }

        }
    }



    private IEnumerator Loading(MovingState nextState)
    {
        currentState = MovingState.Loading;

        yield return new WaitForSeconds(2f);

        currentState = nextState;

    }

}
