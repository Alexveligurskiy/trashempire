﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dump : MonoBehaviour
{

    [SerializeField]
    int Capacity;

    TextMesh text;

    public int currentTrashLoaded;

    [SerializeField]
    int index;

    public string name = "Dump ";

    void Start()
    {
        //currentTrashLoaded = PlayerPrefs.GetInt("currentTrashLoaded1");
        text = GetComponentInChildren<TextMesh>();

        name += index;

        UpdateCounter();
    }


    public int LoadTrash(int trash)
    {

        if (Capacity - currentTrashLoaded >= trash)
        {
            currentTrashLoaded += trash;

            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded1", currentTrashLoaded);
            return 0;
        }
        else
        {
            currentTrashLoaded = Capacity;


            UpdateCounter();
            PlayerPrefs.SetInt("currentTrashLoaded1", currentTrashLoaded);
            return trash - (Capacity - currentTrashLoaded);

        }
    }


    void UpdateCounter()
    {

        text.text = currentTrashLoaded + "/" + Capacity;

    }

}
