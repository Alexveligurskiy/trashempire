﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UI;

public class GameManager : MonoBehaviour
{

    int WorkersAvailable = 1;
    int MaxWorkers=5;

    float time_period;

    [SerializeField]
    Text workers;

    MenuManager menu_manager;
    Dump[] dumps;

    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }

	void Start ()
    {

        menu_manager = FindObjectOfType<MenuManager>();

        dumps = FindObjectsOfType<Dump>();
	}
	



    void Update()
    {

        if (time_period > 0)
        {
            time_period -= Time.deltaTime;
        }
        else
        {
            time_period = 5f;


            CalculateNewCurrency();
        }


    }
	

    private void CalculateNewCurrency()
    {
        int addCoins = 0;

        for(int i=0; i<dumps.Length;i++)
        {
            addCoins += dumps[i].currentTrashLoaded;
        }


        menu_manager._currencies.coins += (addCoins/10);
        menu_manager.SetCurrenciesText();
        menu_manager._currencies.SaveCurrencies();


    }


    public int GetIndex()
    {
        return WorkersAvailable;
    }
	

    public void AddWorkers()
    {



        if (WorkersAvailable<MaxWorkers&&menu_manager._currencies.coins>100f)
        {
            WorkersAvailable += 1;
           
            menu_manager._currencies.coins -= 100f;
            menu_manager.SetCurrenciesText();
            menu_manager._currencies.SaveCurrencies();

        }
  
    }
}
