﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UI {
    public class SettingsManager : MonoBehaviour {
        [Header("Settings Buttons:")]
        public Button returnButton;
        public Button musicButton;
        public Button soundButton;
        public Button restorePurchasesButton;

        [Header("Music and Sound sprites:")]
        public Sprite music;
        public GameObject musicOff;
        public Sprite sound;
        public GameObject soundOff;

        public Image musicImage; 
        public Image soundImage;

        [Header("Manager reference:")]
        public AudioManager _audioManager;
        public MenuManager _menuManager;
        // Use this for initialization
        void Start() {
            SetOnClickSettingsButtons();

        }

        // Update is called once per frame
        void Update() {

        }
        void SetOnClickSettingsButtons() {
            //returnButton.onClick.AddListener(_menuManager.GoPrevPanel);
            musicButton.onClick.AddListener(ToggleMusic);
            soundButton.onClick.AddListener(ToggleSound);
            //restorePurchasesButton.onClick.AddListener();
        }

        public void ToggleMusic() {
            int toggleMusic = PlayerPrefs.GetInt("Music");
            if (toggleMusic == 1) {

                //set on
                _audioManager.ToggleMusic(1);
                PlayerPrefs.SetInt("Music", 0);
                musicImage.sprite = music;
                musicOff.SetActive(false);
            } else {
                //set off
                _audioManager.ToggleMusic(0);
                PlayerPrefs.SetInt("Music", 1);
                musicImage.sprite = music;
                musicOff.SetActive(true);
            }
        }

        public void ToggleSound() {

            int toggleSound = PlayerPrefs.GetInt("Sound");
            if (toggleSound == 1) {
                //set on
                _audioManager.ToggleSound(1);
                PlayerPrefs.SetInt("Sound", 0);
                soundImage.sprite = sound;
                soundOff.SetActive(false);
            } else {
                //set off
                _audioManager.ToggleSound(0);
                PlayerPrefs.SetInt("Sound", 1);
                soundImage.sprite = sound;
                soundOff.SetActive(true);
            }
        }
        public void LoadSoundAndMusic() {
            int toggleMusic = PlayerPrefs.GetInt("Music");
            if (toggleMusic == 1) {
                
                //set on
                _audioManager.ToggleMusic(0);
                musicImage.sprite = music;
                musicOff.SetActive(true);
            } else {
                //set off
                _audioManager.ToggleMusic(1);
                musicImage.sprite = music;
                musicOff.SetActive(false);
            }

            int toggleSound = PlayerPrefs.GetInt("Sound");
            if (toggleSound == 1) {

                //set on
                _audioManager.ToggleSound(0);
                soundImage.sprite = sound;
                soundOff.SetActive(true);
            } else {
                //set off
                _audioManager.ToggleSound(1);
                soundImage.sprite = sound;
                soundOff.SetActive(false);
            }
        }
    }
}