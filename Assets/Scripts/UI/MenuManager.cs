﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
namespace UI
{
    public class MenuManager : MonoBehaviour
    {
        public static MenuManager Instance;
        [Header("Collectables text:")]
        public TextMeshProUGUI coinsText;
        public TextMeshProUGUI trashBagsText;


        [Header("Panels:")]
        public GameObject MenuPanel;
        public GameObject RecyclePanel;
        public GameObject SortingPanel;
        public GameObject StorePanel;
        public GameObject UpgradePanel;

        [Header("MenuPanel objects:")]
        public Button openMenuButton;   
        public Button closeMenuButton;

        [Header("RecyclePanel objects:")]
        public Button openRecycleFactoryButton; 
        public Button closeRecycleFactoryButton;

        [Header("SortingPanel objects:")]
        public Button openSortingFactoryButton;
        public Button closeSortingFactoryButton;

        [Header("StorePanel objects:")]
        public Button openStoreButton;
        public Button closeStoreButton;


        [Header("References:")]
        public Currencies _currencies;
        public SettingsManager _settingManager;

        int firstRun;
        // Use this for initialization
        void Start()
        {
            Instance = this;
            _settingManager.LoadSoundAndMusic();
            AddButtonListeners();
            firstRun = PlayerPrefs.GetInt("savedFirstRun");

            if (firstRun == 0)
            {//Если первый запуск игры то инициализируем по дефолту и сохраняем их в префс
                Debug.Log("First run :" + firstRun);
                firstRun = 1;
                PlayerPrefs.SetInt("savedFirstRun", firstRun);//записываем что игра уже была первый раз запущена

                _currencies.SetCurrenciesByDefault();
                _currencies.SaveCurrencies();
            }
            else
            {//Если игра уже когда то была запущена то просто загружаем из префс
                Debug.Log("First run :" + firstRun);
                _currencies.GetCurrencies();
            }
            SetCurrenciesText();
        }

        // Update is called once per frame
        void Update()
        {

        }
        void AddButtonListeners(){
            openMenuButton.onClick.AddListener(OpenCloseMenu);
            openRecycleFactoryButton.onClick.AddListener(OpenCloseRecycleFactory);
            openSortingFactoryButton.onClick.AddListener(OpenCloseSortingFactory);

            closeMenuButton.onClick.AddListener(OpenCloseMenu);
            closeRecycleFactoryButton.onClick.AddListener(OpenCloseRecycleFactory);
            closeSortingFactoryButton.onClick.AddListener(OpenCloseSortingFactory);

            openStoreButton.onClick.AddListener(OpenCloseStore);

            closeStoreButton.onClick.AddListener(OpenCloseStore);

        }
        public void SetCurrenciesText()
        {
            coinsText.SetText(_currencies.coins.ToString());
            trashBagsText.SetText(_currencies.trashBags.ToString());
        }

        ///////////////////////////////////////////////////

        void OpenCloseRecycleFactory()
        {
            Animator animator = RecyclePanel.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("Open");
                animator.SetBool("Open", !isOpen);
            }
        }

        ///////////////////////////////////////////////////

        void OpenCloseSortingFactory()
        {
            Animator animator = SortingPanel.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("Open");
                animator.SetBool("Open", !isOpen);
            }
        }
        public void OpenCloseStore()
        {
            Animator animator = StorePanel.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("Open");
                animator.SetBool("Open", !isOpen);
            }
        }

        ///////////////////////////////////////////////////
        /// 
        public void OpenCloseMenu()
        {
            Animator animator = MenuPanel.GetComponent<Animator>();
            if(animator != null)
            {
                bool isOpen = animator.GetBool("Open");
                animator.SetBool("Open", !isOpen);
            }
        }
        public void OpenCloseUpgradePanel(){
            Animator animator = UpgradePanel.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("Open");
                animator.SetBool("Open", !isOpen);
            }
        }
        public void GoBackToMenuScene()
        {
            SceneManager.LoadScene("MenuScene");
        }
    }
}