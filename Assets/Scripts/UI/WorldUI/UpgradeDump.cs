﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI;

public class UpgradeDump : MonoBehaviour
{

    [SerializeField]
    Dump dump;
    MenuManager menu_manager;




    void Start()
    {
       
        menu_manager = FindObjectOfType<MenuManager>();


    }



    void OnMouseDown()
    {


        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(dump.name,dump);

    }

}
