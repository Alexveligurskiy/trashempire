﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI;

public class UpgradePoint : MonoBehaviour
{

    [SerializeField]
    TakeUpPoint point;
    MenuManager menu_manager;




    void Start()
    {
        
        menu_manager = FindObjectOfType<MenuManager>();


    }



    void OnMouseDown()
    {


        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(point.name,point);

    }

}
