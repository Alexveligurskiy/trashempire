﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI;

public class UpgradeTruck : MonoBehaviour
{


    CityTruck city_truck;
    MenuManager menu_manager;




    void Start()
    {
        city_truck = FindObjectOfType<CityTruck>();

        menu_manager = FindObjectOfType<MenuManager>();


    }



    void OnMouseDown()
    {


        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(city_truck.name,city_truck);

    }

}
