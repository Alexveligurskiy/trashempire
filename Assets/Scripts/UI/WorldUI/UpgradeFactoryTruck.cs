﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI;

public class UpgradeFactoryTruck : MonoBehaviour
{

    [SerializeField]
    FactoryTruck factory;
    MenuManager menu_manager;




    void Start()
    {
        
        menu_manager = FindObjectOfType<MenuManager>();


    }



    void OnMouseDown()
    {


        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(factory.name,factory);

    }

}
