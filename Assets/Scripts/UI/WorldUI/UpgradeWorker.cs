﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI;

public class UpgradeWorker : MonoBehaviour
{

    [SerializeField]
    Worker worker;
    MenuManager menu_manager;




    void Start()
    {
        
        menu_manager = FindObjectOfType<MenuManager>();

    }



    void OnMouseDown()
    {


        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(worker.name,worker);

    }

}
