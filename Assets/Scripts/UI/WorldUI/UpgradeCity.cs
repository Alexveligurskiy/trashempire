﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UI;

public class UpgradeCity : MonoBehaviour
{


    City city;
    MenuManager menu_manager;




    void Start()
    {
        city = FindObjectOfType<City>();

        menu_manager = FindObjectOfType<MenuManager>();

       
    }



    void OnMouseDown()
    {

        
        menu_manager.OpenCloseUpgradePanel();
        UpgradeManager.current.SetUpgradeProps(city.name,city);

    }

 
    }
