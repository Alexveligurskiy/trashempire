﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UI{
    public class AudioManager : MonoBehaviour {
        

        [Header("Manager reference:")]
        public MenuManager _menuManager;

        [Header("Audio:")]
        public AudioSource Sound, Music;

        //public AudioClip starHitSound, levelCompleteSfx, clickSound;


        void Start() {
            
        }
        /*
        public void PlayStarHit() {
            characterSound.PlayOneShot(starHitSound, 1.0f);
        }
        public void PlayLevelComplete() {
            characterSound.PlayOneShot(levelCompleteSfx, 1.0f);
        }

        public void PlayClickSound() {
            characterSound.PlayOneShot(clickSound, 1.0f);
        }
*/
        public void ToggleMusic(int _toggle) {
            if (_toggle == 0) {
                //Music.mute = true;
                Debug.Log("Music Off");
            } else{
                //Music.mute = false;
                Debug.Log("Music On");
            }
                
        }
        public void ToggleSound(int _toggle) {
            if (_toggle == 0) {
                //Sound.mute = true;
                Debug.Log("Sound Off");
            } else {
                //Sound.mute = false;
                Debug.Log("Sound On");
            }
        }
    }
}