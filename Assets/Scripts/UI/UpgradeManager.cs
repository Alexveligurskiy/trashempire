﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using UI;
public class UpgradeManager : MonoBehaviour
{

    public GameObject workersAmount;
    public GameObject capacityAmount;
    public GameObject walkSpeed;
    public GameObject uploadSpeed;

    public TextMeshProUGUI upgradeName;

    public TextMeshProUGUI workersAmountName;
    public TextMeshProUGUI capacityAmountName;
    public TextMeshProUGUI walkSpeedName;
    public TextMeshProUGUI uploadSpeedName;

    public TextMeshProUGUI workersAmountNow;
    public TextMeshProUGUI capacityAmountNow;
    public TextMeshProUGUI walkSpeedNow;
    public TextMeshProUGUI uploadSpeedNow;


    public TextMeshProUGUI workersAmountPlus;
    public TextMeshProUGUI capacityAmountPlus;
    public TextMeshProUGUI walkSpeedPlus;
    public TextMeshProUGUI uploadSpeedPlus;

    public TextMeshProUGUI upgradeCostText;
    public Button upgradeButton;


    public static UpgradeManager current;

    void Awake()
    {
        current = this;

    }


	void Start ()
    {
        SetUpgradeButton();
	}
	
	

    public void SetUpgradesTexts()
    {
        workersAmountNow.SetText("");
        capacityAmountNow.SetText("");
        walkSpeedNow.SetText("");
        uploadSpeedNow.SetText("");

        workersAmountPlus.SetText("");
        capacityAmountPlus.SetText("");
        walkSpeedPlus.SetText("");
        uploadSpeedPlus.SetText("");

        upgradeCostText.SetText("Cost: " );
    }


    public void SetUpgradeButton()
    {
        upgradeButton.onClick.AddListener(Upgrade);



    }


    public void SetUpgradeProps(string name, Worker worker)
    {

        float price_speed = 35f;
        float price_max_capacity = 45f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);


        workersAmountName.SetText("Walk Speed");
        capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_speed+"$");
        capacityAmountNow.SetText(price_max_capacity+"$");

    }

    public void SetUpgradeProps(string name,City city)
    {
        upgradeName.SetText("Upgrade "+name);

        float price_per_time = 14f;
        float price_max_capacity = 88f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);


        workersAmountName.SetText("Trash Per Second");
        capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_per_time + "$");
        capacityAmountNow.SetText(price_max_capacity + "$");

    }


    public void SetUpgradeProps(string name, Factory factory)
    {
        upgradeName.SetText("Upgrade " + name);

        //float price_speed = 35f;
        float price_max_capacity = 40f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);

        workersAmountName.SetText("Max Capacity");
       // capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_max_capacity + "$");
        //capacityAmountNow.SetText(price_max_capacity + "$");

    }

    public void SetUpgradeProps(string name, TakeUpPoint point)
    {
        upgradeName.SetText("Upgrade " + name);

        //float price_speed = 35f;
        float price_max_capacity = 78f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);

        workersAmountName.SetText("Max Capacity");
        // capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_max_capacity + "$");
        //capacityAmountNow.SetText(price_max_capacity + "$");

    }

    public void SetUpgradeProps(string name, Dump dump)
    {
        upgradeName.SetText("Upgrade " + name);

        upgradeName.SetText("Upgrade " + name);

        //float price_speed = 35f;
        float price_max_capacity = 120f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);

        workersAmountName.SetText("Max Capacity");
        // capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_max_capacity + "$");
        //capacityAmountNow.SetText(price_max_capacity + "$");

    }

    public void SetUpgradeProps(string name, CityTruck cityTruck)
    {
        upgradeName.SetText("Upgrade " + name);

        float price_speed = 130f;
        float price_max_capacity = 150f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);


        workersAmountName.SetText("Ride Speed");
        capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_speed + "$");
        capacityAmountNow.SetText(price_max_capacity + "$");

    }

    public void SetUpgradeProps(string name, FactoryTruck factoryTruck)
    {
        upgradeName.SetText("Upgrade " + name);

        float price_speed = 130f;
        float price_max_capacity = 150f;

        walkSpeed.SetActive(false);
        uploadSpeed.SetActive(false);

        capacityAmount.SetActive(true);
        workersAmount.SetActive(true);

        upgradeName.SetText("Upgrade " + name);


        workersAmountName.SetText("Ride Speed");
        capacityAmountName.SetText("Max Capacity");

        workersAmountNow.SetText(price_speed + "$");
        capacityAmountNow.SetText(price_max_capacity + "$");


    }


    public void Upgrade()
    {
        Currencies.Instance.coins -= 280;
        Currencies.Instance.SaveCurrencies();
        MenuManager.Instance.SetCurrenciesText();
    }
}
