﻿using UnityEngine;
using System.Collections;
namespace UI
{
    public class Currencies : MonoBehaviour
    {
        public static Currencies Instance;
        public float coins { get; set; }
        public int trashBags { get; set; }
        // Use this for initialization
        int firstRun;
        void Start()
        {
            Instance = this;

        }
        public void SetCurrenciesByDefault(){
            coins = 0;
            trashBags = 0;
        }
        public void SaveCurrencies()
        {
            PlayerPrefs.SetInt("trashBags", trashBags);
            PlayerPrefs.SetFloat("coins", coins);
        }
        public void GetCurrencies()
        {
            coins = PlayerPrefs.GetFloat("coins");
            trashBags = PlayerPrefs.GetInt("trashBags");
        }
    }
}